package com.project.interview.bfabian.interviewproject.views;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.interview.bfabian.interviewproject.R;
import com.project.interview.bfabian.interviewproject.adapters.PhotoItemAdapter;
import com.project.interview.bfabian.interviewproject.model.PictureItem;
import com.project.interview.bfabian.interviewproject.server.LoadImage;
import com.project.interview.bfabian.interviewproject.server.RestClient;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView mPhotoRecyclerView;
    private PhotoItemAdapter adapter;
    private List<PictureItem> mPhotoInList = new ArrayList<>();
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
        initViews();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        try {
            trimCache(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void trimCache(Context context) {
        try {
            File file = new File(Environment.getExternalStorageDirectory()
                    .getPath(), "TestFolder");
            if (file != null && file.isDirectory()) {
                deleteDir(file);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    private void initViews(){
        mPhotoRecyclerView = (RecyclerView) findViewById(R.id.fragment_home_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mPhotoRecyclerView.setLayoutManager(layoutManager);
        mPhotoRecyclerView.setAdapter(adapter);
        loadJSON();

    }

    private void loadJSON(){
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://beta.json-generator.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestClient restClient = retrofit.create(RestClient.class);

        Call<List<PictureItem>> call = restClient.getData();


        call.enqueue(new Callback<List<PictureItem>>() {
            @Override
            public void onResponse(Call<List<PictureItem>> call, Response<List<PictureItem>> response) {

                switch (response.code()) {
                    case 200:
                        mPhotoInList = response.body();
                        int index = 0;
                        for(PictureItem pictureItem: mPhotoInList){
                            String url = pictureItem.getPicture();
                            String name = pictureItem.getId();
                            LoadImage cargarImagen = new LoadImage(context);
                            cargarImagen.execute(url,name);
                            mPhotoInList.get(index).setPicture(LoadImage.getUri(name));
                            index++;
                        }
                        adapter = new PhotoItemAdapter(context,mPhotoInList);
                        mPhotoRecyclerView.setAdapter(adapter);

                        //downloadImage(response.body());
                        break;
                    case 401:
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<PictureItem>> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }

}
