package com.project.interview.bfabian.interviewproject.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.interview.bfabian.interviewproject.R;
import com.project.interview.bfabian.interviewproject.model.PictureItem;
import com.project.interview.bfabian.interviewproject.server.LoadImage;
import com.squareup.picasso.Picasso;

import java.io.File;

public class PhotoActivity extends AppCompatActivity {
    public static final String PHOTO_DESCRIPTION = "photoDescription";

    TextView txtViewTitle;
    TextView txtViewComment;
    TextView txtViewPublishedAt;
    ImageView imgViewPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_description);
        String jsonPhotoDescription = getIntent().getStringExtra(PHOTO_DESCRIPTION);
        PictureItem pictureItem = deserializeFromJson(jsonPhotoDescription,PictureItem.class);
        initViews(pictureItem);
    }

    private void initViews(PictureItem pictureItem){

        txtViewTitle = (TextView) findViewById(R.id.pd_title);
        txtViewComment = (TextView) findViewById(R.id.pd_comment);
        txtViewPublishedAt = (TextView) findViewById(R.id.pd_publishedAt);
        imgViewPicture = (ImageView) findViewById(R.id.pd_picture);

        txtViewTitle.setText(pictureItem.getTitle());
        txtViewComment.setText(pictureItem.getComment());
        txtViewPublishedAt.setText(pictureItem.getPublishedAt());

        File file = new File(LoadImage.getUri(pictureItem.getId()));
        if(file.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            imgViewPicture.setImageBitmap(myBitmap);
        }

    }


    public static <T> T deserializeFromJson(String jsonFrom, Class<T> to) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonFrom, to);
    }



}