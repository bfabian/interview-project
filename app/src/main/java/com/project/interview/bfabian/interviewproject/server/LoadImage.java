package com.project.interview.bfabian.interviewproject.server;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.project.interview.bfabian.interviewproject.views.HomeActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by bfabian on 25/06/17.
 */

public class LoadImage extends AsyncTask<String, Void, Bitmap> {
    ProgressDialog pDialog;
    Context mContext;

    public LoadImage(Context context) {
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Cargando Imagenes");
        pDialog.setCancelable(true);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.show();

    }

    @Override
    protected Bitmap doInBackground(String... params) {
        // TODO Auto-generated method stub
        Log.i("doInBackground" , "Entra en doInBackground");
        String url = params[0];
        String name = params[1];
        Bitmap imagen = descargarImagen(url,name);
        return imagen;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);


        //imgImagen.setImageBitmap(result);
        pDialog.dismiss();
    }

    private Bitmap descargarImagen (String imageHttpAddress, String name){
        URL imageUrl = null;
        Bitmap imagen = null;
        try{
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            imagen = BitmapFactory.decodeStream(conn.getInputStream());
            saveImageFile(imagen,name);
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return imagen;
    }
    public String saveImageFile(Bitmap bitmap, String name) {
        FileOutputStream out = null;
        String filename = getFilename(name);
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private static String getFilename(String name) {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "TestFolder");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + name + ".jpg");
        return uriSting;
    }

    public static String getUri(String name){
        String filename = getFilename(name);
        return filename;
    }}
