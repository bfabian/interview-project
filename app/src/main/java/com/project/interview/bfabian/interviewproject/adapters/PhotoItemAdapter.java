package com.project.interview.bfabian.interviewproject.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.interview.bfabian.interviewproject.R;
import com.project.interview.bfabian.interviewproject.model.PictureItem;
import com.project.interview.bfabian.interviewproject.views.PhotoActivity;

import java.io.File;
import java.util.List;


public class PhotoItemAdapter extends RecyclerView.Adapter<PhotoItemAdapter.PhotoHolder>{

    private List<PictureItem> mListPictureItem;
    private Context contextParent;
    public PhotoItemAdapter(Context context,List<PictureItem> pictureItems) {
        contextParent = context;
        mListPictureItem = pictureItems;
    }

    @Override
    public PhotoItemAdapter.PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext()).inflate(R.layout.picture_card_view,parent,false);
        return new PhotoHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoItemAdapter.PhotoHolder photoHolder, int position) {
        try {
            photoHolder.bindPhoto(mListPictureItem.get(position));
        }catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public int getItemCount() {
        return mListPictureItem.size();
    }

    public class PhotoHolder extends RecyclerView.ViewHolder {
        private TextView publishedAt;
        private ImageView picture;


        public PhotoHolder(View itemView) {
            super(itemView);
            picture = (ImageView) itemView.findViewById(R.id.picture);
            publishedAt = (TextView) itemView.findViewById(R.id.publishedAt);
        }


        public void bindPhoto(final PictureItem photoIn) {
            publishedAt.setText(photoIn.getPublishedAt());
            File file = new File(photoIn.getPicture());
            if(file.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                picture.setImageBitmap(myBitmap);
            }


            picture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(contextParent,PhotoActivity.class);
                    String stringJson = serializeToJson(photoIn);
                    intent.putExtra(PhotoActivity.PHOTO_DESCRIPTION,stringJson);
                    contextParent.startActivity(intent);
                }
            });

        }

    }
    public static <T> String serializeToJson(T object) {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(object);
    }
}
