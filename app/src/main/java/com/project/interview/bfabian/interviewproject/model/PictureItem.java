package com.project.interview.bfabian.interviewproject.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PictureItem {
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("publishedAt")
    @Expose
    private String publishedAt;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PictureItem{" +
                "picture='" + picture + '\'' +
                ", publishedAt='" + publishedAt + '\'' +
                ", comment='" + comment + '\'' +
                ", title='" + title + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}