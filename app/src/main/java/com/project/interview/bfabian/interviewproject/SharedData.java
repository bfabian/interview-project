package com.project.interview.bfabian.interviewproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SharedData {
    public static void saveData(Context con, String variable, String data) {
        data = encrypt(data);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
        prefs.edit().putString(variable, data).commit();
    }

    public static String getData(Context con, String variable, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
        String data = prefs.getString(variable, defaultValue);
        data = decrypt(data);
        if (data != null) {
            return data;
        } else {
            return defaultValue;
        }
    }


    public static String encrypt(String clearText) {
        byte[] encryptedText;
        try {
            SecretKeySpec ks = new SecretKeySpec(getKey(), "AES");
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, ks);
            encryptedText = c.doFinal(clearText.getBytes("UTF-8"));
            return Base64.encodeToString(encryptedText, Base64.DEFAULT);
        } catch (Exception e) {
            return null;
        }
    }

    public static String decrypt(String encryptedText) {
        byte[] clearText = null;
        try {
            SecretKeySpec ks = new SecretKeySpec(getKey(), "AES");
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, ks);
            clearText = c.doFinal(Base64.decode(encryptedText, Base64.DEFAULT));
            return new String(clearText, "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    private static byte[] getKey() {
        byte[] seed = "carlos_go_to_incode".getBytes();
        KeyGenerator kg;
        try {
            kg = KeyGenerator.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        SecureRandom sr;
        try {
            sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        } catch (Exception e) {
            return null;
        }
        sr.setSeed(seed);
        kg.init(128, sr);
        SecretKey sk = kg.generateKey();
        byte[] key = sk.getEncoded();
        return key;
    }


    public static class Tags {
        public static final String DATA_IS_DOWNLOADED = "downloaded";//check if the data (jSON) is already downloaded
    }
    public static class Values{
        public static final String FALSE = "0";
        public static final String TRUE = "1";
    }
}
