package com.project.interview.bfabian.interviewproject.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by carloshernandezsantes on 12/8/16.
 * Abstract class used for create Activities than have just one Fragment
 */


public abstract class SingleFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(getFragmentContainer());
        if (fragment == null) {
            fragment = createFragment();
            fragmentManager.beginTransaction()
                    .add(getFragmentContainer(), fragment)
                    .commit();
        }
    }

    protected abstract Fragment createFragment();//For pass an instance of the fragment

    protected abstract int getLayout();//for pass the Layout Container

    protected abstract int getFragmentContainer(); //for pass the Fragment Container,it most to be a view child of LayoutContainer

}

