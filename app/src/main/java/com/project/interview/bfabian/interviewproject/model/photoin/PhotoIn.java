package com.project.interview.bfabian.interviewproject.model.photoin;

import java.io.Serializable;

/**
 * Created by carloshernandezsantes on 12/8/16.
 */

public class PhotoIn implements Serializable {
    private String picture;//image url
    private String publishedAt;// date "dayName, month dd, yyyy HH:MM PM
    private String comment;//comments about the photo
    private String title;
    private String _id; //photo id


    public PhotoIn() {
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
