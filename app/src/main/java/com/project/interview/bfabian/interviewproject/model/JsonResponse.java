package com.project.interview.bfabian.interviewproject.model;

/**
 * Created by bfabian on 25/06/17.
 */

public class JsonResponse {
    PictureItem [] pictureItems;

    public PictureItem[] getPictureItems() {
        return pictureItems;
    }

    public void setPictureItems(PictureItem[] pictureItems) {
        this.pictureItems = pictureItems;
    }
}
