package com.project.interview.bfabian.interviewproject.server;

import com.project.interview.bfabian.interviewproject.model.JsonResponse;
import com.project.interview.bfabian.interviewproject.model.PictureItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.GET;

/**
 * Created by bfabian on 25/06/17.
 */

public interface RestClient {
    @GET("api/json/get/EkphH5xyM/")
    Call<List<PictureItem>> getData();
}
