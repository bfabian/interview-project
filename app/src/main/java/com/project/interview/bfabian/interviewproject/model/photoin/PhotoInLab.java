package com.project.interview.bfabian.interviewproject.model.photoin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carloshernandezsantes on 12/13/16.
 */

public class PhotoInLab {

    private static PhotoInLab sPhotoInLab;

    private static ArrayList<PhotoIn> sPhotoIns;

    public static PhotoInLab set() {
        if (sPhotoInLab == null) {
            sPhotoInLab = new PhotoInLab();
        }
        return sPhotoInLab;
    }

    public static void add(PhotoIn photoIn){
        sPhotoIns.add(photoIn);
    }

    public static PhotoInLab set(List<PhotoIn> photoIns){
        if (sPhotoInLab == null) {
            sPhotoInLab = new PhotoInLab();
            sPhotoIns = new ArrayList<>(photoIns);
        }
        return sPhotoInLab;
    }

    private PhotoInLab() {
        sPhotoIns = new ArrayList<>();
    }

    public static List<PhotoIn> getPhotoIns() {
        return sPhotoIns;
    }

    public static PhotoIn getPhotoIn(String id) {
        for (PhotoIn photoIn : sPhotoIns) {
            if (photoIn.get_id().equals(id)) {
                return photoIn;
            }
        }
        return null;
    }


}
